import socket
from struct import *
import pcapy
import sys
import random
from scapy.all import *

def main(argv):
#argv[0] = Script name,
#argv[1] = interface,
#argv[2] = Server IP,
#argv[3] = Destination Port

    if(len(argv) != 5):
        print "Usage : ./client.py <Interface> <Server IP> <Src Port> <Dst Port>"
        exit(1)

    interface = argv[1]
    server = argv[2]
    client_port = int(argv[3])
    server_port = int(argv[4])

    print "Generating random nonce"
    #Generate a pseudorandom 32-bit number
    nonce = random.randint(0, pow(2, 32) - 1)

    print "Nonce = " + str(nonce)
    #Pack as unsigned integer (I) , using network byte order (!)
    packed_nonce_network_order = pack('!I', nonce)

    received_nonce = send_nonce(packed_nonce_network_order, interface, server, client_port, server_port)
    #received_nonce = receive_nonce(interface, client_port)


    if(received_nonce == nonce + 1):
        print "Incremented nonce successfully received"
        exit(0)
    else:
        print "Incorrect nonce receieved"
        exit(1)

def send_nonce(UDP_payload, interface, server_ip, s_port, d_port):
    #Run ARP query
    print "Running ARP query on IP : " + server_ip
    ans,unans=srp(Ether(dst="ff:ff:ff:ff:ff:ff ")/ARP(pdst=server_ip),timeout=2)
    # ans.summary(lambda (s, r): r.sprintf("%Ether.src%"))    # Debugging
    if len(ans) == 0:
        print "ARP failed"
        exit(1)
    dest_mac = ans[0][1][0][0].src    # This is how you get the MAC from ARP
    #This is how Scapy works
    print "Destination MAC : " + dest_mac

    #Construct the frame
    udp_segment = UDP(sport=s_port, dport=d_port)/UDP_payload
    ip_packet = IP(dst=server_ip, proto=17)/udp_segment
    eth_frame = Ether(type=0x88b5,dst=dest_mac)/ip_packet

    #Assemble the frame
    str(eth_frame)

    #Send it
    sendp(eth_frame)
    nonce = start_listening(interface, s_port, eth_frame[0].src)

    print "Received nonce : " + str(nonce)
    #ans = srp1(eth_frame)
    #ans.show()
    return nonce
    #print "Received " + str(nonce)

def start_listening(interface, listen_port, my_mac):

    print "Listening on: %s" % interface

    # Open a live capture
    reader = pcapy.open_live(interface, 1500, 1, 0)
    print "Filtering packets headed for " + my_mac
    reader.setfilter('ether dst ' + my_mac)
    while True:
        try:
            (header, payload) = reader.next()
        except pcapy.PcapError:
            continue
        print "Received packet"
        (found, data, reply_ip, reply_port) = parse_packet(payload, listen_port)
        if(found):                #UDP packet found
            print "Length: " +str(len(data))
            nonce = unpack('!I', data)[0]
            return nonce


def parse_packet(packet, listen_port):
    eth_length = 14
    eth_header = packet[:eth_length]
    eth = unpack('!6s6sH', eth_header)
    eth_protocol = eth[2]
    print eth_protocol
    if(eth_protocol == 0x88b5):
        #Matches our defined ETH protocol type
        #Parse IP header
        #take first 20 characters for the ip header
        ip_header = packet[eth_length:20 + eth_length]
        print "Matching ethernet header found! Checking IP header now"
        #now unpack them :)
        iph = unpack('!BBHHHBBH4s4s', ip_header)

        version_ihl = iph[0]
        version = version_ihl >> 4
        ihl = version_ihl & 0xF

        iph_length = ihl * 4

        ttl = iph[5]
        protocol = iph[6]
        s_addr = socket.inet_ntoa(iph[8])
        d_addr = socket.inet_ntoa(iph[9])

        print 'Version : ' + str(version) + ' IP Header Length : ' + str(ihl) + ' TTL : ' + str(ttl) + ' Protocol : ' + str(protocol) + ' Source Address : ' + str(s_addr) + ' Destination Address : ' + str(d_addr)

        if (protocol == 17) :    #UDP packet
            print "UDP header found"
            u = iph_length + eth_length
            udph_length = 8
            udp_header = packet[u:u + 8]

            #now unpack them :)
            udph = unpack('!HHHH', udp_header)

            source_port = udph[0]
            dest_port = udph[1]
            length = udph[2]
            checksum = udph[3]

            print 'Source Port : ' + str(source_port) + ' Dest Port : ' + str(dest_port) + ' Length : ' + str(length) + ' Checksum : ' + str(checksum)


            if(dest_port != listen_port):
                print "Received on non-listening port"
                return (False, None, None, None)

            h_size = eth_length + iph_length + udph_length
            print "Offset of UDP data is : " + str(h_size)
            #get four bytes of data from the packet
            data = packet[h_size:h_size + 4]

            print 'UDP Data : ' + data
            return (True, data, s_addr, source_port)

    return (False, None, None, None)

if __name__ == "__main__":
    main(sys.argv)
